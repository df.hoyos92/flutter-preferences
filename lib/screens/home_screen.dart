import 'package:flutter/material.dart';
import 'package:flutter_preferences/preferences/shared_preferences.dart';

import '../widgets/widgets.dart';

class HomeScreen extends StatelessWidget {
  static const String routerName = 'Home';

  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Home'),
      ),
      drawer: const SideMenu(),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text('isDarkmode: ${Preferences.isDarkmode}'),
          const Divider(),
          Text('Genero: ${ Preferences.gender == 1 ? 'Masculino' : 'Femenino'}'),
          const Divider(),
          Text('Nombre de Usuario: ${Preferences.gender}')
        ],
      ),
    );
  }
}
